import sys,argparse
import base64
from datetime import datetime
import os
import shutil

import numpy as np
import socketio
import eventlet
import eventlet.wsgi
from PIL import Image
from flask import Flask
from io import BytesIO

import tensorflow as tf
import cv2

from model import Model
import utils

# Communication with Udacity
sio = socketio.Server()
app = Flask(__name__)

# Neural network parameters
graph_path = 'cow-notcow-model.meta'
checkpoints_path = './'

# AEBS parameters
cow_threshold = 0.999

@sio.on('telemetry')
def telemetry(sid, data):
    if data:

        # Data from Udacity
        steering_angle = float(data["steering_angle"])
        throttle = float(data["throttle"])
        speed = float(data["speed"])

        # Current image from center camera
        stream = BytesIO(base64.b64decode(data["image"]))
        data = np.fromstring(stream.getvalue(), dtype=np.uint8)
        image = cv2.imdecode(data, 1)

        # Run cow detector
        pred = nn.predict(image)

        cow_detected = pred[0][0] > cow_threshold

        steering_angle = 0 # Not used by our contorller
        throttle = 1.0

        if cow_detected:
            # Apply brake
            print('Cow detected: ' + str(pred))
            throttle = -100

            # Stop braking if car is not moving
            if abs(speed) < 0.5:
                throttle = 0
        else:
            print('Cow NOT detected')

        try:
            send_control(steering_angle, throttle, pred[0][0],pred[0][1])
        except Exception as e:
            print(e)

    else:
        sio.emit('manual', data={}, skip_sid=True)


@sio.on('connect')
def connect(sid, environ):
    print("connect ", sid)
    send_control(0,0,0,0)

def send_control(steering_angle, throttle, cow , notcow):
    sio.emit(
        "steer",
        data={
            'steering_angle': steering_angle.__str__(),
            'throttle': throttle.__str__(),
            'cow': cow.__str__(),
            'notcow': notcow.__str__()
        },
        skip_sid=True)


if __name__ == '__main__':

    if len(sys.argv)>1:
        graph_path=sys.argv[1]
        checkpoints_path = sys.argv[2]

    with tf.Session() as sess:

        # Initialize neural network
        nn = Model()
        nn.init(graph_path, checkpoints_path, sess)

        # wrap Flask application with engineio's middleware
        app = socketio.Middleware(sio, app)
        # deploy as an eventlet WSGI server
        eventlet.wsgi.server(eventlet.listen(('', 4567)), app)
