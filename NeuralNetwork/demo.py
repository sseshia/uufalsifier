import tensorflow as tf
from model import Model
import cv2

graph_path = 'cow-notcow-model.meta'
checkpoints_path = './'

with tf.Session() as sess:
    nn = Model()
    nn.init(graph_path, checkpoints_path, sess)
    image = cv2.imread('./data/test/cow/center_2017_10_02_17_41_15_686.jpg')
    print(nn.predict(image))
