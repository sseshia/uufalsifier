﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BreachHarnessStatic : MonoBehaviour {
 
    private GameObject m_Car;
	private GameObject m_Cow;

	private UnityStandardAssets.Vehicles.Car.CarController m_CarController; 
	private UnityStandardAssets.Vehicles.Car.CarAIControl m_CarAIControl; 
    private System.IO.StreamWriter m_file;

	// Modification space
	private Vector3 init_pos;

	private float next_scene_time;
	private float period = 1f;

	private float x_min = -3;
	private float x_max = 3;
	private float z_min = -3;
	private float z_max = 3;
	private float rot_y_min = 0;
	private float rot_y_max = 360;


    // Use this for initialization
    void Awake () {

        // Find the player car and its controller
        m_Car = GameObject.Find("CarTraining");
		m_Cow = GameObject.Find("Marguerite");
		init_pos = m_Cow.transform.position;
		m_CarController = m_Car.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
		m_CarAIControl = m_Car.GetComponent<UnityStandardAssets.Vehicles.Car.CarAIControl>();

        // turn on recording 
        m_CarController.IsRecording = true;

        // count simulation
        BreachScript.sim_count++;

        // Create trace file 
		next_scene_time = 0.0f;
        m_file = new System.IO.StreamWriter(BreachScript.trace_name);

    }

  
    // FixedUpdate is called once per dt -- do stuff and stop after max_time
    void FixedUpdate()
    {

		// Alter the actual scene
		if (Time.time > next_scene_time) {
			
			next_scene_time += period;

			float x_pos = Random.Range (x_min, x_max);
			float z_pos = Random.Range (z_min, z_max);
			float y_rot = Random.Range (rot_y_min, rot_y_max);

			Vector3 pos = new Vector3(x_pos, 0, z_pos);
			Vector3 rot = new Vector3 (0, y_rot, 0);

			Rigidbody rb_cow = m_Cow.GetComponent<Rigidbody> ();

			rot.Set(0f, y_rot, 0f);
			Quaternion deltaRotation = Quaternion.Euler(rot);
			rb_cow.MoveRotation(rb_cow.rotation * deltaRotation);
			rb_cow.MovePosition(init_pos + pos);

		}

		// Save trajectory
        if (Time.timeSinceLevelLoad < BreachScript.max_time)
        {
			m_Car = GameObject.Find("CarTraining");
			m_Cow = GameObject.Find("Marguerite");

			string text_trace_line = Time.timeSinceLevelLoad + " "
			                         + m_CarController.CurrentSpeed + " "
			                         + m_CarController.CurrentSteerAngle + " "
			                         + m_CarAIControl.CowProba + " "
			                         + m_CarAIControl.NotCowProba + " "
			                         + m_Car.transform.position.x + " "
			                         + m_Car.transform.position.y + " "
			                         + m_Car.transform.position.z + " "
			                         + m_Cow.transform.position.x + " "
			                         + m_Cow.transform.position.y + " "
			                         + m_Cow.transform.position.z + " "
			                         + m_Cow.transform.rotation.eulerAngles.x + " "
			                         + m_Cow.transform.rotation.eulerAngles.y + " "
			                         + m_Cow.transform.rotation.eulerAngles.z;

            m_file.WriteLine(text_trace_line); 

        }
            else
        {
            // Close trace file
            m_file.Close(); 

            SceneManager.LoadScene("BreachScene");
        }
    }  
} 
