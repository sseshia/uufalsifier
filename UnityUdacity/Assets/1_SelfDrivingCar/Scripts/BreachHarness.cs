﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BreachHarness : MonoBehaviour {
 
    private GameObject m_Car;
	private GameObject m_Cow;

	private UnityStandardAssets.Vehicles.Car.CarController m_CarController; 
	private UnityStandardAssets.Vehicles.Car.CarAIControl m_CarAIControl; 
    private System.IO.StreamWriter m_file;

    // Use this for initialization
    void Awake () {

        // Find the player car and its controller
        m_Car = GameObject.Find("CarTraining");
		m_Cow = GameObject.Find("Marguerite");
		m_CarController = m_Car.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
		m_CarAIControl = m_Car.GetComponent<UnityStandardAssets.Vehicles.Car.CarAIControl>();

        // turn on recording 
        m_CarController.IsRecording = true;

        // Assign parameter values
        m_CarController.MaxSpeed = BreachScript.param_dict["max_speed"];
        m_CarController.MaxSteerAngle = BreachScript.param_dict["max_steer_angle"];
        // Time acceleration
        Time.timeScale = BreachScript.param_dict["time_scale"];
        Time.fixedDeltaTime = 0.02F/Time.timeScale;
		// Cow position and rotation
		Vector3 cow_pos = new Vector3(BreachScript.param_dict["cow_xpos"], 0, 0);
		Vector3 cow_rot = new Vector3(0, BreachScript.param_dict["cow_yrot"], 0);
		m_Cow.transform.position += cow_pos;
		m_Cow.transform.eulerAngles += cow_rot;
		// Cow color
		byte red = (byte) BreachScript.param_dict["cow_rcol"];
		byte green = (byte) BreachScript.param_dict["cow_gcol"];
		byte blue = (byte) BreachScript.param_dict["cow_bcol"];
		byte alpha = 255; // transparency
		m_Cow.GetComponent<Renderer> ().material.color = new Color32 (red, green, blue, alpha);

        // count simulation
        BreachScript.sim_count++;

        // Create trace file 
         m_file = new System.IO.StreamWriter(BreachScript.trace_name);



    }

  
    // FixedUpdate is called once per dt -- do stuff and stop after max_time
    void FixedUpdate()
    {
        
        if (Time.timeSinceLevelLoad < BreachScript.max_time)
        {
			m_Car = GameObject.Find("CarTraining");
			m_Cow = GameObject.Find("Marguerite");

			string text_trace_line = Time.timeSinceLevelLoad + " "
				+ m_CarController.CurrentSpeed + " "
			    + m_CarController.CurrentSteerAngle + " "
			    + m_CarAIControl.CowProba + " "
			    + m_CarAIControl.NotCowProba + " "
			    + m_Car.transform.position.x + " "
			    + m_Car.transform.position.y + " "
			    + m_Car.transform.position.z + " "
			    + m_Cow.transform.position.x + " "
			    + m_Cow.transform.position.y + " "
			    + m_Cow.transform.position.z;
            m_file.WriteLine(text_trace_line); 
        }
            else
        {
            // Close trace file
            m_file.Close(); 

            SceneManager.LoadScene("BreachScene");
        }
    }  
} 