﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BreachScript : MonoBehaviour
{

	public string breach_unity_path;
    static public float max_time = 1.0F;
    static public float max_speed = 10.0F;
    static public int sim_count = 0;
    static public int max_sim_count = 0;
    static public Dictionary<string, float> param_dict = new Dictionary<string, float>();
    static public float[,] parameters;
    static public string trace_name;
    static public string trace_name_prefix;

    // Use this for initialization
    void Start()
    {
		string dir_separator = Path.DirectorySeparatorChar.ToString();

//		#if (!UNITY_EDITOR)
//		string[] args = Environment.GetCommandLineArgs(); 
//		breach_unity_path = args[(args.Length)-1];
//		Debug.Log("Breach Unity path:"+breach_unity_path);
//		#endif

		string file_params = breach_unity_path + dir_separator + "unity_params.csv";
		string file_time = breach_unity_path + dir_separator + "unity_time.csv";
        string scene;

		trace_name_prefix = breach_unity_path + dir_separator + "Traces" + dir_separator + "trace";
        param_dict.Clear();

	    

        // Read time
		string time_text = System.IO.File.ReadAllText(file_time);
		max_time = float.Parse(time_text);
        
        param_dict.Add("Time",max_time);

        // Read parameters 
        string[] lines = System.IO.File.ReadAllLines(file_params);

        // Dimensions 
        string[] text_params = lines[0].Split(',');

        max_sim_count = lines.Length-1;
        int max_param_count = text_params.Length;

        if (sim_count == 0)
        {
            parameters = new float[max_sim_count, max_param_count];
            max_sim_count = -1;
            foreach (string line in lines)
            {
                if (max_sim_count >= 0)
                {
                    string[] val_params = line.Split(',');

                    int param_count = 0;
                    foreach (string val_param in val_params)
                    {
                        Debug.Log(val_params);
                        parameters[max_sim_count, param_count] = float.Parse(val_param);
                        param_count++;
                    }
                }
                max_sim_count++;
            }
        }

        if (sim_count < max_sim_count)
        {
            int param_count = 0;
            foreach (string text_param in text_params)
            {
                string param_name = text_params[param_count];
                param_dict[param_name] = parameters[sim_count,param_count];
                param_count++;
            }
            
            trace_name = trace_name_prefix+"_"+(sim_count+1);
            for (int i_param = 0; i_param < max_param_count; i_param++)
            {
                trace_name = trace_name + "_" + parameters[sim_count, i_param];
            }
            Debug.Log("Running simulation " + sim_count + "/"+ (max_sim_count-1) + "writing trace to " + trace_name);

            int scene_number = (int) param_dict["scene_number"];
           
            switch (scene_number) {
                case 0:
                    scene = "LakeTrackBreach";
                    break;
                case 1:
                    scene = "LakeTrackBreachCow";
                    break;
				case 2:
					scene = "LakeTrackBreachStatic";
					break;
                default:
                    scene = "LakeTrackBreach";
                    break;
            }

            SceneManager.LoadScene(scene);
        }
        else
        {
            QuitRun();
        }
    }

    public void QuitRun()
    {
        // save any game data here
		#if UNITY_EDITOR
		// Application.Quit() does not work in the editor so
		// UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
		UnityEditor.EditorApplication.isPlaying = false;
		#endif
        Application.Quit();
    }


}
