%% Creates BreachUnity interface
Bu = BreachUnity({'speed', 'steer_angle', 'cow', 'notcow', 'car_x', 'car_y', 'car_z', 'cow_x', 'cow_y', 'cow_z', 'cow_rot_x', 'cow_rot_y', 'cow_rot_z'},...
    {'batch_mode', 'scene_number','time_scale',...
    'max_speed', 'max_steer_angle',...
    'cow_xpos', 'cow_yrot',...
    'cow_rcol', 'cow_gcol', 'cow_bcol'},...
    [0 2 1 50 30 0 0 255 255 255]);
 
%% Sets Parameters
p1 = 'max_speed';
p2 = 'max_steer_angle';
p3 = 'cow_xpos';
p4 = 'cow_yrot';
p5 = 'cow_rcol';
p6 = 'cow_gcol';
p7 = 'cow_bcol';

Bu.SetParamRanges({p3,p4, p5, p6, p7}, [-3 3 ; 0 360; 0 255; 0 255; 0 255]);
Bu.SetTime(0:.02:20);

%% Sample
Bu.QuasiRandomSample(1);

%% Run Unity and plot collected signals
Bu.Sim();
Bu.PlotSignals(); 

