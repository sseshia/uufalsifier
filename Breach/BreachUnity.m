classdef BreachUnity < BreachSystem
    
    properties
        breach_unity_path 
        param_file  = 'unity_params.csv'
        cfg_time = 'unity_time.csv';
        trace_folder = 'Traces'
        unity_exe = ['..' filesep '..' filesep 'UnityUdacity' filesep 'Builds' filesep 'BreachRunLake']
        unity_exe_opt = ''
    end
    
    methods
        function this = BreachUnity(signals, params, p0)
            this.Sys = CreateExternSystem('UnitySystem', signals, params,p0);
            this.P = CreateParamSet(this.Sys);
     
            fid = fopen(this.param_file, 'w');
            this.breach_unity_path = fileparts(which('BreachUnity'));
            fclose(fid);
            
            if ~ispc 
                this.unity_exe = ['..' filesep '..' filesep 'UnityUdacity' filesep 'Builds' filesep 'BreachRunLake.x86_64'];
            end
            
            %  Default domains
            for ip = this.P.DimP
                this.Domains(ip) = BreachDomain();
            end
            this.Domains(this.P.DimX+1).type = 'bool'; % this is for batch_mode
            
        end
        
        function Sim(this, tspan)
            if  nargin==1
                tspan = this.GetTime();
            end
            % Write configuration files with time and parameters
            csvwrite(this.cfg_time, tspan(end),0,0);
            
            % Write parameter names
            fid = fopen(this.param_file, 'w');
            params = this.P.ParamList(this.Sys.DimX+1:this.Sys.DimP);
            cellfun(@(c)(fprintf(fid,[c ','])), params(1:end-1));
            fprintf(fid, [params{end} '\n' ]);
            dlmwrite(this.param_file, this.P.pts(this.Sys.DimX+1:this.Sys.DimX+numel(params),:)' , '-append' );
            fclose(fid);
            % Clean Trace folder
            delete([this.trace_folder filesep 'trace*'])
            
            % Launch Unity game with Breach harness
            if this.GetParam('batch_mode', 1)
                this.unity_exe_opt = '-batchmode';
            else
                this.unity_exe_opt = '';
            end
            
            system([this.unity_exe ' ' this.unity_exe_opt this.breach_unity_path]);
            
            %  Read traces
            files = dir([this.trace_folder filesep 'trace*']);
            nb_traces = numel(files);
            
            this.P.Xf =zeros(size(this.P.pts));
            for it = 1: numel(files)
                files = dir([this.trace_folder filesep 'trace_' num2str(it) '_*']);
                fname = [pwd filesep this.trace_folder filesep files(1).name];
                traji = load_traj(fname);
                traji.param = this.P.pts(1:this.P.DimP, it)';
                traj{it} = traji;
                this.P.Xf(1:this.P.DimX,it) = traji.X(:,end);
            end
            this.P.traj = traj;
            this.P.traj_ref = 1:nb_traces;
            this.P.traj_to_compute= [];
            
        end
        
        function run_aebs(this)
             python_interpreter = 'C:\ProgramData\Anaconda3\envs\tensorflow130\python';
             NN_path = ['..' filesep 'NeuralNetwork'];
             aebs_script = [NN_path filesep 'aebs.py' ];
             NN_model = [NN_path filesep 'cow-notcow-model.meta' ];
             cmd  = [python_interpreter ' ' aebs_script ' ' NN_model ' ' NN_path];
             disp(cmd);
             system(cmd);
        end
        
    end
end
