## Falsification Procedure based on Udacity-Unity Self-Driving Simulator

This is a falsification procedure developed on top of the Udacity Self-driving simulator based on the Unity game engine.

Follow all instructions in the README in the UnityUdacity folder.

Install Matlab and the Breach Matlab toolkit: http://github.com/decyphir/breach

Install Python and TensorFlow (see README in NeuralNetwork folder)

The steps below have been tested on Windows (8.1 and 10) and with Matlab 2016b.

### Steps

1. Open Unity

2. Open project (select the UnityUdacity folder)

3. Navigate to Assets -> 1_SelfDrivingCar -> Scenes. From displayed Assets, click on BreachScene, select BreachRunner, and then enter path to the "Breach" folder in your working copy of the repository

4. File -> Build Settings -> make sure all the "Scenes in Build" with "Breach" in the title are selected. Click Build. Create a folder "Builds" and Name executable as BreachRunLake 

5. Start Matlab, open test_Unity.m. In Matlab, make sure Breach is in the path. Run InitBreach. Create an empty folder named Traces within the Breach folder.

6. Run the Neural Network based Automatic Emergency Braking System (AEBS) example code in Python. Look under the NeuralNetwork folder, run "python aebs.py" at the command prompt. Wait for tensorflow to get initialized.

7. Then at the Matlab prompt, run test_Unity.m. This varies the scene parameters from within Breach and renders the resulting scenes in Unity. The Python program will generate output from the neural network classifier.

8. Run test_falsif_Unity.m. This runs the falsification procedure that searches for a violation of the "absence of collision" property specified in the .stl file.



